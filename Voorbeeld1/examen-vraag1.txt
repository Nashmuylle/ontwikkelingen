

Je bent een eigen game aan het maken.
 In je game kan een speler allerlei items verzamelen die aan handelaars verkocht kunnen worden.
 Dergelijke items hebben geen complexe gemeenschappelijke logica, maar hebben wel allemaal een zekere waarde (uitgedrukt als een aantal goudstukken).

Een speler moet een item eerst oppakken voor het in zijn inventaris komt. Items hebben een omschrijving en wanneer de speler een item oppakt, kan er een bepaald effect zijn:

    drankjes zijn 100 goudstukken waard, hebben de omschrijving "Een drankje". en doen niets speciaals
    vervloekte talismans zijn 500 goudstukken waard, hebben de omschrijving "Ziet er eng uit". en halveren de gezondheid van de speler
    gezegende talismans zijn 1000 goudstukken waard, hebben de omschrijving "Geeft je moed". en verhogen gezondheid van de speler met 10, maar niet boven zijn maximum