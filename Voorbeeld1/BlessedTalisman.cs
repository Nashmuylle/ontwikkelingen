using System;
namespace Voorbeeld1 {
    class BlessedTalisman : IItem //we implementeren de interface Iitem
    {
        private string description = "Geeft je moed"; //De beschrijving van het item
        private int value = 1000; //De waarde van het item
        public string GetDescription(){
            return this.description; //We geven de omschrijving van het item terug
        }
        public int GetValue(){
            return this.value; //We geven de waarde van het item terug
        }
        public int ReactToPickup(int Health, int maxHealth){
            //De speler mag niet meer levens krijgen dan de maximumlevens.
            //In dit geval is dit maxhealth - 10 omdat hij 10 levens bijkrijgt. (vermijden van vb: 91 health + 10 = 101) Dit mag niet
            if(Health > (maxHealth-10)){
                Console.WriteLine("Je hebt teveel levens om te healen"); 
            }
            else{
                return Health +10; //Als hij toch mag healen returnen we zijn levens+ de 10 extra levens die hij krijgt van het item
            }
            return Health; //We zouden normaal nooit tot dit stuk moeten komen maar moest er toch iets fout gaan 
            // dan returnen we gewoon de spelers zijn levens.
        }

    }

}