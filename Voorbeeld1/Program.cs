﻿using System;
using System.Collections.Generic;

namespace Voorbeeld1 {
    class Program {
        public static void Main() {
            Player player = new Player(); //We maken een nieuwe speler aan
            IItem potion1 = new Potion(); //We maken een nieuwe potion aan
            IItem potion2 = new Potion();
            IItem cursed = new CursedTalisman(); //We maken een nieuwe CursedTalisman aan 
            IItem blessed = new BlessedTalisman(); //We maken een nieuwe BlessedTalisman aan
            player.PickUpItem(potion1); //De speler pakt een potion op
            Console.WriteLine($"Speler heeft nog {player.GetHealth()} health."); //Hier geven we weer aan de speler wat er gebeurt dankzij het item.
            player.PickUpItem(potion2); //De speler pakt weer een potion op
            Console.WriteLine($"Speler heeft nog {player.GetHealth()} health.");
            player.PickUpItem(cursed); //De speler pakt een CursedTalisman op
            Console.WriteLine($"Speler heeft nog {player.GetHealth()} health.");
            player.PickUpItem(blessed); //De speler pakt een BlessedTalisman op
            Console.WriteLine($"Speler heeft nog {player.GetHealth()} health.");
            foreach(IItem item in player.GetItems()) { //We gaan door de list met een foreach loop zodat we elk item kunnen tonen
                Console.WriteLine($"{item.GetDescription()} Dit is {item.GetValue()} goudstukken waard."); //We tonen alle items die de speler bezit
            }
        }
    }
}