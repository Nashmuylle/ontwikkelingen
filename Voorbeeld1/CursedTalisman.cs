using System;
namespace Voorbeeld1 {
    class CursedTalisman : IItem //we implementeren de interface Iitem
    {
        private string description = "Ziet er eng uit"; // De omschrijving van het item
        private int value = 500; //De waarde van het item
        public string GetDescription(){
            return this.description; //We geven de omschrijving van het item terug
            
        }
        public int GetValue(){
            return this.value; //We geven de waarde van het item terug
        }
        public int ReactToPickup(int Health, int maxHealth){
            return Health/2;  // Als de speler het item oppakt worden zijn levens door 2 gedeeld.
            
            //Opzich kan de speler nooit doodgaan want zijn levens worden gewoon telkens door 2 gedeeld. 
        }

    }

}