namespace Voorbeeld1 {
    class Potion : IItem //we implementeren de interface IItem
    {
        private string description = "Een drankje"; //De omschrijving van het item
        private int value = 100; //De waarde van het item
        public string GetDescription(){
            return this.description; //We geven de omschrijving terug.
            
        }
        public int GetValue(){
            return this.value; //We geven de waarde terug.
        }
        public int ReactToPickup(int Health, int maxHealth){
            return Health; //Het moet niets met de speler zijn levens doen dus we geven gewoon zijn volledige levens terug.
        }

    }

}