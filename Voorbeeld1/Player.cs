using System.Collections.Generic;

namespace Voorbeeld1 {
    class Player {
        private List<IItem> items; //We maken een list aan van de interface met alle items die de speler heeft opgepakt. 
        //(list omdat de lengte niet op voorhand hoeft geweten te zijn dan)
        private int health; // De speler zijn levens
        private int maxHealth; //De speler zijn maximumlevens

        public Player() { //elke nieuwe speler krijgt deze zaken. 
            items = new List<IItem>(); //Een nieuwe list met items
            this.SetMaxHealth(100); //We zetten de speler zijn maximumlevens op 100
            this.SetHealth(100); //De speler krijgt standaard 100 levens bij het starten.
        }

        public int GetHealth() {
            return this.health; //We geven de levens van de speler terug.
        }

        public void SetHealth(int health) {
            this.health = health; //We zetten deze speler zijn levens
        }

        public int GetMaxHealth() {
            return this.maxHealth; //We geven de maximumlevens voor de speler
        }

        public void SetMaxHealth(int maxHealth) {
            this.maxHealth = maxHealth; //We zette, de maximumlevens voor de speler
        }

        public List<IItem> GetItems() {
            return this.items; //Geef de speler zijn items terug.
        }

        public void PickUpItem(IItem item) { //Een methode die een item oppakt. Het item wordt meegegeven als parameter in Program.cs
            items.Add(item); //We voegen een item toe aan de speler zijn item list.
            this.health = item.ReactToPickup(this.health, this.maxHealth); //we zorgen ervoor dat de speler zijn levens reageren op het oppakken van de levens
        }
    }
}