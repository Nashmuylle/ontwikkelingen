using System.Collections.Generic;

namespace Voorbeeld1 {
    interface IItem //We maken een interface aan die we gaan implementeren op de klasses van de items
    {
        string GetDescription(); //Een methode die de omschrijving gaat teruggeven
        int GetValue(); //Een methode die de waarde gaat teruggeven
        int ReactToPickup(int health, int maxHealth ); //Een methode die reageert bij het oppakken van het item en iets uniek doet.

    }

}