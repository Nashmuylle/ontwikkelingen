using System;

namespace Voorbeeld3 {
    class LinkedNumberList {
        private int number; //Het nummer van het huidige element
        private LinkedNumberList successor; //Het volgende element naast het huidige

        public LinkedNumberList(int number, LinkedNumberList successor) { //We geven in de parameters het nummer van het huidig element mee en ook de opvolger
            this.number = number; //We zetten dit nummer gelijk aan het meegegeven nummer van de parameter
            this.successor = successor; //We zetten deze opvolger gelijk aan de opvolger die is meegegeven in de parameter
        }

        public LinkedNumberList GetSuccessor() {
            return this.successor; //We geven de opvolger mee
        }

        public int GetNumber() {
            return this.number; //We geven het nummer van het huidig element mee
        }

        public bool HasNext() {
            return this.successor != null; //Een test om te zien of we aan het laatste element zitten. Deze zou geen opvolger mogen hebben.
        }

    }
}