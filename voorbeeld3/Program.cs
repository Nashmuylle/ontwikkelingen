﻿using System;

namespace Voorbeeld3
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedNumberList lst4 = new LinkedNumberList(4, null); // een lijst met alleen het elementje 4 in en geen opvolger
            LinkedNumberList lst3 = new LinkedNumberList(3,lst4); // een lijst met alleen het elementje 3 in en als opvolger lst4
            LinkedNumberList lst2 = new LinkedNumberList(2,lst3); // een lijst met alleen het elementje 2 in en als opvolger lst3
            LinkedNumberList lst1 = new LinkedNumberList(1,lst2); // een lijst met alleen het elementje 1 in en als opvolger lst2
            LinkedNumberList current = lst1; //Het startpunt om door de gelinkte lijst te gaan
            while(current.HasNext()) { 
                //De while loopt blijft lopen zolang er een opvolger is
                Console.WriteLine($"Huidig element is: {current.GetNumber()}");
                current = current.GetSuccessor(); //we zetten hier het volgende element klaar door het gelijk te stellen aan de opvolger
                
            }
            // laatste element heeft geen opvolger
            Console.WriteLine($"Huidig element is: {current.GetNumber()}"); //Dit geeft het laatste element zonder opvolger weer. 
            //Daarom staat het niet mee in de while loop
        }
    }
}
