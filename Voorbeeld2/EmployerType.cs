namespace Voorbeeld2 {
    enum EmployerType { //We maken een enum aan om fouten te vermijden. Er zijn maar 2 werkgevers die mogen gebruikt worden.
        MANAGER,
        ORGANIZER
    }
}