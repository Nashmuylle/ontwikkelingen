using System;
namespace Voorbeeld2 {
     class Degree {
         //We maken hier een klasse aan waarvan we gaan overerven in andere om zo de verschillende soorten diploma's te maken
         //Elk diploma bevat sowieso wel een paar standaardzake vandaar dus het gebruik van een ouderklasse en kindklasses
        protected string name; //De naam van de diploma houder

        public void SetName(){ //We stellen de naam van de diplomahouder in.
            Console.WriteLine("Voor wie is dit Diploma?");
            this.name = Console.ReadLine(); //De gebruiker kan de naam ingeven voor de diplomahouder
        }

        public string GetName(){
            return this.name; //We geven de naam van de diplomahouder terug
        }

        public virtual string GetEmployerResponse(EmployerType Type) //We geven de enum EmployerType mee zodat zeker de juiste werkgever wordt gebruikt.
        {
            return "response"; //respone komt voort vanuit de derived classes. in de klasse zelf zouden we nooit iets moeten returnen
        }

    }
}