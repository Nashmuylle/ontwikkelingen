﻿using System;

namespace Voorbeeld2 {
    class Program {
        public static void Main() {
            Degree d1 = new ITDegree(); //We maken een nieuw ITdiploma aan, aan de hand van de ouderklasse
            Degree d2 = new ManagementDegree(); //We maken een nieuw management diploma aan, aan de hand van de ouderklasse
            Degree d3 = new CateringDegree(); //We maken een nieuw catering diploma aan, aan de hand van de ouderklasse
            d1.SetName(); //We geven een diplomahouder aan de diplomas
            d2.SetName();
            d3.SetName();
            //Hierna gaan we de reacties printen aan de gebruiker.
            Console.WriteLine($"De reactie van een manager op het diploma van {d1.GetName()}: {d1.GetEmployerResponse(EmployerType.MANAGER)}");
            Console.WriteLine($"De reactie van een organisator op het diploma van {d1.GetName()}: {d1.GetEmployerResponse(EmployerType.ORGANIZER)}");
            Console.WriteLine($"De reactie van een manager op het diploma van {d2.GetName()}: {d2.GetEmployerResponse(EmployerType.MANAGER)}");
            Console.WriteLine($"De reactie van een organisator op het diploma van {d2.GetName()}: {d2.GetEmployerResponse(EmployerType.ORGANIZER)}");
            Console.WriteLine($"De reactie van een manager op het diploma van {d3.GetName()}: {d3.GetEmployerResponse(EmployerType.MANAGER)}");
            Console.WriteLine($"De reactie van een organisator op het diploma van {d3.GetName()}: {d3.GetEmployerResponse(EmployerType.ORGANIZER)}");
        }
    }
}
