namespace Voorbeeld2 {
    class ManagementDegree : Degree {
        public override string GetEmployerResponse(EmployerType Type){ //We passen de methode voor de response van de werkgever aan. (vandaar override)
        //We maken een logsiche test aan om te zien welke werkgever op het diploma reageert en telkens met zijn eigen soort reactie dan
            if(Type == EmployerType.MANAGER)
            {
                return "HOERA"; 
            }
            else if(Type == EmployerType.ORGANIZER)
            {
                return "BOE";
            }
            return "Geen werkgever gevonden"; // Als de logische test toch faalt dan is dit omdat er geen werkgever is om reactie te geven.
            //Dit zou nooit kunnen/mogen voorvallen
        }
        
    }
}